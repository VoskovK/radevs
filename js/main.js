// Slick Initialization
$(".sliderSlick__box").slick({
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 575,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }
  ]
});

$(document).ready(function () {
  $("input[type='radio']").click(function () {
      var sim = $("input[type='radio']:checked").val();
      //alert(sim);
      if (sim < 3) {
          $(".myratings").css("color", "red");
          $(".myratings").text(sim);
      } else {
          $(".myratings").css("color", "green");
          $(".myratings").text(sim);
      }
  });
});

let heartIcons = document.getElementsByClassName("bi-heart-fill");

for (i = 0; i < heartIcons.length; i++) {
  heartIcons[i].addEventListener("click", function () {
      for (i = 0; i < heartIcons.length; i++) {
          heartIcons[i].classList.remove("selected");
      }
      this.classList.toggle("selected");
  });
}

const ratingStars = [...document.getElementsByClassName("rating_star")];

function executeRating(stars) {
  const starClassActive = "rating_star bi bi-star-fill";
  const starClassInactive = "rating_star bi bi-star-fill";
  const starsLength = stars.length;
  let i;
  stars.map((star) => {
      star.onclick = () => {
          i = stars.indexOf(star);

          if (star.className === starClassInactive) {
              for (i; i >= 0; --i) stars[i].className = starClassActive;
          } else {
              for (i; i < starsLength; ++i) stars[i].className = starClassInactive;
          }
      };
  });
}
executeRating(ratingStars);
